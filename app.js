const express = require("express");
const multer = require("multer");
const fs = require("fs");
const app = express();
app.set('views', './views');
app.set('view engine', 'pug');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '.jpg';
        cb(null, file.fieldname + '-' + uniqueSuffix)
    }
})

let upload = multer({ storage: storage })

const uploaded_files = [];

app.get('/', (request, response) => {
    const path = './public/uploads';
    fs.readdir(path, (err, items) => {
     response.render('index', {
         title: "Welcome to Kenziegram!",
         images: items
     });
    });
})

app.post('/upload', upload.single('photo'), (request, response) => {
    uploaded_files.push(request.file.filename);
    console.log("Uploaded: " + request.file.filename);
    response.render('upload', {
        title: 'Uploaded Photo!',
        images: uploaded_files
    })
});

app.use(express.static('./public'));

const port = 3000

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))